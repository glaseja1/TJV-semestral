<%-- 
    Document   : StudentInfo
    Created on : 13.12.2017, 22:31:06
    Author     : Honza
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Info</title>
        
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/semestral_1_0/css/default.css?random=<?php echo uniqid(); ?>" />
        
        <script src="http://localhost:8080/semestral_1_0/js/jquery.js"></script> 
        <script>
            $(function () {
                $("#includedContent").load("http://localhost:8080/semestral_1_0/include/topNavBar.html");
            });
        </script>        
    </head>
    <body>
        <div id="includedContent"></div>
        <h1>Student Info</h1>
        <form action="http://localhost:8080/semestral_1_0/ServiceServlet" method="POST">
            <table>
                <tr>
                    <td>Student ID</td>
                    <td><input type="text" name="id" value="${addStudent.ID}" 
                               required="true"
                               requiredMessage="empty">           </td>         
                </tr>
                <tr>
                    <td>Student First name</td>
                    <td><input type="text" name="firstName" value="${addStudent.firstName}" 
                               required="true"
                               requiredMessage="empty">     </td>                  
                </tr>
                <tr>
                    <td>Student Last name</td>
                    <td><input type="text" name="lastName" value="${addStudent.lastName}" 
                               required="true"
                               requiredMessage="empty">       </td>                
                </tr>
                <tr>
                    <td>Student Age</td>
                    <td><input type="text" name="age" value="${addStudent.age}" 
                               required="true"
                               requiredMessage="empty">           </td>         
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="hidden" name="ent" value="student"> 
                        <input type="submit" name="action" value="Add">       
                        <input type="submit" name="action" value="Edit">       
                        <input type="submit" name="action" value="Delete">       
                        <input type="submit" name="action" value="Search">       
                    </td>

                </tr>
            </table>

        </form>

        <br>
        
    </body>
</html>
