/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.StudentClient;
import client.TeacherClient;
import entities.Student;
import entities.Students;
import entities.Teacher;
import entities.Teachers;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.XMLParserStudent;
import utils.XMLParserTeacher;

/**
 *
 * @author Honza
 */
public class ServletTeacherProcesser extends ServletBaseProcessor {

    public ServletTeacherProcesser()
    {
        super(new TeacherClient());
    }
    
    @Override
    protected void onActionAdd(PrintWriter out, HttpServletRequest request) {
        String ID = request.getParameter("id");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");

        Teacher s = new Teacher(firstName, lastName);

        out.println("<p>Adding teacher</p>");
        client.asTeacherClient().create_JSON(s);
    }

    @Override
    protected void onActionEdit(PrintWriter out, HttpServletRequest request) {
        String ID = request.getParameter("id");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");

        Teacher s = new Teacher(firstName, lastName);

        out.println("<p>Adding teacher</p>");
        client.asTeacherClient().create_JSON(s);
    }

    @Override
    protected void onActionDelete(PrintWriter out, HttpServletRequest request) {
        String ID = request.getParameter("id");

        out.println("<p>Deleting teacher</p>");
        client.asTeacherClient().remove(ID);
    }

    @Override
    protected void onActionSearch(PrintWriter out, HttpServletRequest request, HttpServletResponse response,
            String html) {
        //Students students = new Students();
        //try {
        Teachers X = new XMLParserTeacher().parse(html);

        out.println("<h2>All teachers available</h2>");
        out.println("<table border=\"1\">");
        out.println("<tr><th>ID</th><th>First name</th><th>Last name</th><th>Age</th></tr>");
        for (Teacher st : X.getTeachers()) {
            out.println("<tr>");
            out.println("<th>");
            out.println(st.getId());
            out.println("</th>");
            out.println("<th>");
            out.println(st.getFirstName());
            out.println("</th>");
            out.println("<th>");
            out.println(st.getLastName());
            out.println("</th>");
            out.println("</tr>");
        }
        out.println("</table>");
    }
}
