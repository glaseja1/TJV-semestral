/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.ClientBase;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Honza
 */
public abstract class ServletBaseProcessor {

    protected ClientBase client;

    protected ServletBaseProcessor(ClientBase client) {
        this.client = client;
    }

    public final void processAction(PrintWriter out, HttpServletRequest request,
            HttpServletResponse response, String action) {
        if ("add".equals(action)) {
            onActionAdd(out, request);
        } else if ("edit".equals(action)) {
            onActionEdit(out, request);
        } else if ("delete".equals(action)) {
            onActionDelete(out, request);
        } else if ("search".equals(action)) {
            String html = client.findAll_XML_getHtml(out);
            onActionSearch(out, request, response, html);
        } else {
            out.println("<p>Unidentified action '" + action + "'");
        }
    }

    protected abstract void onActionAdd(PrintWriter out, HttpServletRequest request);

    protected abstract void onActionDelete(PrintWriter out, HttpServletRequest request);

    protected abstract void onActionEdit(PrintWriter out, HttpServletRequest request);

    protected abstract void onActionSearch(PrintWriter out, HttpServletRequest request,
            HttpServletResponse response, String HTML_XML);
}
