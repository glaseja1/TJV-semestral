/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.StudentClient;
import client.VerbClient;
import entities.Student;
import entities.Students;
import entities.Verb;
import entities.Verbs;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.XMLParserStudent;
import utils.XMLParserVerb;

/**
 *
 * @author Honza
 */
public class ServletVerbProcesser extends ServletBaseProcessor{
    public ServletVerbProcesser()
    {
        super(new VerbClient());
    }

    @Override
    protected void onActionAdd(PrintWriter out, HttpServletRequest request) {
       String ID = request.getParameter("id");
        String inf = request.getParameter("infinitive");
                
        Verb s = new Verb(inf);
        
        out.println("<p>Adding verb</p>");
        client.asVerbClient().create_JSON(s);
    }

    @Override
    protected void onActionDelete(PrintWriter out, HttpServletRequest request) {
        String ID = request.getParameter("id");
        client.asVerbClient().remove(ID);
    }

    @Override
    protected void onActionEdit(PrintWriter out, HttpServletRequest request) {
        String ID = request.getParameter("id");
        String i = request.getParameter("infinitive");

        Verb s = new Verb(i);
        client.asStudentClient().edit_JSON(s, ID);
    }

    @Override
    protected void onActionSearch(PrintWriter out, HttpServletRequest request, HttpServletResponse response, String html) {
        Verbs verbs = new XMLParserVerb().parse(html);
            
            out.println("<h2>All verbs available</h2>");
            out.println("<table border=\"1\">");
            out.println("<tr><th>ID</th><th>Infinitive</th></tr>");
            for(Verb st : verbs.getVerbs())
            {
                out.println("<tr>");
                out.println("<th>");
                out.println(st.getId());
                out.println("</th>");
                out.println("<th>");
                out.println(st.getInfinitive());
                out.println("</th>");
                out.println("</tr>");
            }
            out.println("</table>");
    }
     
}
