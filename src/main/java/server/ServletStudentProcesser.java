/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import client.StudentClient;
import client.TeacherClient;
import entities.Student;
import entities.Students;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.XMLParserStudent;

/**
 *
 * @author Honza
 */
public class ServletStudentProcesser extends ServletBaseProcessor {
   public ServletStudentProcesser()
    {
        super(new StudentClient());
    }
    @Override
    protected void onActionAdd(PrintWriter out, HttpServletRequest request) {
        
        String ID = request.getParameter("id");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        int age = 0;
        try {
            age = Integer.parseInt(request.getParameter("age"));
        } catch (Exception e) {
            out.println("<p>Age must be numerical</p>");
            return;
        }
        
        Student s = new Student(firstName, lastName, age);
        
        out.println("<p>Adding student</p>");
        client.asStudentClient().create_JSON(s);
    }

    @Override
    protected void onActionDelete(PrintWriter out, HttpServletRequest request) {
         String ID = request.getParameter("id");
        client.asStudentClient().remove(ID);
    }

    @Override
    protected void onActionEdit(PrintWriter out, HttpServletRequest request) {
        String ID = request.getParameter("id");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");

        int age = 0;
        try {
            age = Integer.parseInt(request.getParameter("age"));
        } catch (Exception e) {
            out.println("<p>Age must be numerical</p>");
            return;
        }
        Student s = new Student(firstName, lastName, age);
        client.asStudentClient().edit_JSON(s, ID);
    }

    @Override
    protected void onActionSearch(PrintWriter out, HttpServletRequest request, HttpServletResponse response,
            String html) {
            Students students = new XMLParserStudent().parse(html);
            
            out.println("<h2>All studetns available</h2>");
            out.println("<table border=\"1\">");
            out.println("<tr><th>ID</th><th>First name</th><th>Last name</th><th>Age</th></tr>");
            for(Student st : students.getStudents())
            {
                out.println("<tr>");
                out.println("<th>");
                out.println(st.getId());
                out.println("</th>");
                out.println("<th>");
                out.println(st.getFirstName());
                out.println("</th>");
                out.println("<th>");
                out.println(st.getLastName());
                out.println("</th>");
                out.println("<th>");
                out.println(st.getAge());
                out.println("</th>");
                out.println("</tr>");
            }
            out.println("</table>");
    }
}
