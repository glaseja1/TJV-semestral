/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import javax.ws.rs.ClientErrorException;

/**
 *
 * @author Honza
 */
public class ClientBase {
    private String urlForXml;
    
    protected ClientBase(String urlForXml)
    {
        this.urlForXml = urlForXml;
    }
    public TeacherClient asTeacherClient()
    {
        return (TeacherClient)this;
    }
    public StudentClient asStudentClient()
    {
        return (StudentClient)this;
    }
      public VerbClient asVerbClient()
    {
        return (VerbClient)this;
    }
    public String findAll_XML_getHtml(PrintWriter out) throws ClientErrorException {

        String target = urlForXml;
        StringBuffer buffer = new StringBuffer();
        try {
            URL url = new URL(target);           
            Scanner s = new Scanner(url.openStream());
                        
            while (s.hasNext())
                buffer.append(s.nextLine()); 
            
        } catch (MalformedURLException e) {
            return "Invalid url " + target;
        } catch (IOException e) {
            return "IO exception";
        }
        return buffer.toString();       
    }
}
