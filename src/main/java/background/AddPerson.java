package background;

import bussines.StudentFacade;
import entities.Student;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Honza
 */
@Named(value = "addStudent")
@RequestScoped
public class AddPerson {

    private String firstName;
    private String lastName;
    private int age;
    private Long ID;

    @Inject
    StudentFacade facade;

    public AddPerson() {
    }

    public String add() {
        facade.create(new Student(firstName, lastName, age));
        return "index?faces-redirect=true";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

}
