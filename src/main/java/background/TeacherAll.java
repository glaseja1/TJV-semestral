/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package background;

import bussines.StudentFacade;
import bussines.TeacherFacade;
import entities.Student;
import entities.Teacher;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Honza
 */
@Named(value = "teacherAll")
@Dependent
public class TeacherAll {

    @Inject
    TeacherFacade facade;

    public TeacherAll() {
    }

    public List<Teacher> getAll() {
        return facade.findAll();
    }
}
