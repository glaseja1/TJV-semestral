/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package background;

import bussines.StudentFacade;
import bussines.TeacherFacade;
import entities.Student;
import entities.Teacher;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Honza
 */
@Named(value = "addTeacher")
@RequestScoped
public class AddTeacher {
     private String firstName;
     private String lastName;
     private Long ID;

    @Inject
    TeacherFacade facade;

    public AddTeacher() {
    }

    public String add() {
        facade.create(new Teacher(firstName, lastName));
        return "index?faces-redirect=true";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
    
    
}
