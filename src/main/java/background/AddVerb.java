package background;

import bussines.StudentFacade;
import bussines.VerbFacade;
import entities.Student;
import entities.Verb;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Honza
 */
@Named(value = "addVerb")
@RequestScoped
public class AddVerb {

    private String infinitive;
    private Long ID;

    @Inject
    VerbFacade facade;

    public AddVerb() {
    }

    public String add() {
        facade.create(new Verb(infinitive));
        return "index?faces-redirect=true";
    }

    public String getInfinitive() {
        return infinitive;
    }

    public void setInfinitive(String infinitive) {
        this.infinitive = infinitive;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

}
