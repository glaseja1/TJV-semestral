/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package background;

import bussines.StudentFacade;
import bussines.VerbFacade;
import entities.Student;
import entities.Verb;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Honza
 */
@Named(value = "verbAll")
@Dependent
public class VerbAll {

    @Inject
    VerbFacade facade;

    public VerbAll() {
    }

    public List<Verb> getAll() {
        return facade.findAll();
    }
}
