/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package background;

import bussines.StudentFacade;
import entities.Student;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Honza
 */
@Named(value = "studentAll")
@Dependent
public class StudentAll {

    @Inject
    StudentFacade facade;

    public StudentAll() {
    }

    public List<Student> getAll() {
        return facade.findAll();
    }
}
