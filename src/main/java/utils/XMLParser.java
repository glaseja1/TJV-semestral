/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Honza
 */
public abstract class XMLParser<T> {

    private String expectedXmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";

    private String allEntitiesTag;
    private String OneEntityTag;
    
    protected XMLParser(String allEntitiesTag, String OneEntityTag)
    {
        this. allEntitiesTag = allEntitiesTag;
        this. OneEntityTag = OneEntityTag;
        
    }
    private String cutOffXmlHeader(String xml) {
        if (!containsGoodHeader(xml)) {
            return xml;
        }

        return xml.substring(expectedXmlHeader.length(), xml.length());
    }

    private boolean containsGoodHeader(String xml) {
        if (xml.startsWith(expectedXmlHeader)) {
            return true;
        }
        return false;
    }

    protected String getWhatIsInside(String xml, String tagContent) {
        //split by students
        System.out.println(xml);
        String[] entities = xml.split("<" + tagContent + ">");

        /*for(String yy : entities)
            System.out.println("ENTITY: " + yy);*/
        if (entities.length != 2) {
            throw new RuntimeException();
        }

        return entities[1].substring(0, entities[1].indexOf("</" + tagContent + ">"));
    }
    protected abstract T onProcessSingleEntity(String oneEntityInnerXml);
    
    protected int parseInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (Exception x) {
        }
        return -1;
    }
    protected Long parseLong(String input) {
        try {
            return Long.parseLong(input);
        } catch (Exception x) {
        }
        return -1L;
    }
    protected List<T> parseT(String xml) {
        xml = cutOffXmlHeader(xml);
        xml = getWhatIsInside(xml, allEntitiesTag);

        //split by students
        String[] entities = xml.split("<"+ OneEntityTag+">");

        /*for(String yy : entities)
            System.out.println("ENTITY: " + yy);*/
        List<T> studentC = new LinkedList<T>();
        for (String i : entities) {
            if (i.equals("")) {
                continue;
            }

            //cut off the end tag
            i = i.substring(0, i.indexOf("</"+ OneEntityTag +">"));

            //System.out.println("ENTITY: " + i); 
            T SE = onProcessSingleEntity(i);
            
            studentC.add(SE);
        }
        return studentC;
    }
}
