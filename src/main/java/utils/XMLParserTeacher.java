/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import entities.Student;
import entities.Students;
import entities.Teacher;
import entities.Teachers;
import java.util.List;

/**
 *
 * @author Honza
 */
public class XMLParserTeacher  extends XMLParser<Teacher> {

    public XMLParserTeacher() {
        super("teachers", "teacher");
    }

    @Override
    protected Teacher onProcessSingleEntity(String oneEntityInnerXml) {
        Teacher SE = new Teacher();
        SE.setId(parseLong(getWhatIsInside(oneEntityInnerXml, "id")));
        SE.setFirstName(getWhatIsInside(oneEntityInnerXml, "firstName"));
        SE.setLastName(getWhatIsInside(oneEntityInnerXml, "lastName"));
        return SE;
    }

    public Teachers parse(String xml) {
        List<Teacher> l = super.parseT(xml);
        Teachers st = new Teachers();
        st.setTeachers(l);
        return st;
    }
}
