/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import entities.Student;
import entities.Students;
import java.util.List;

/**
 *
 * @author Honza
 */
public class XMLParserStudent extends XMLParser<Student> {

    public XMLParserStudent() {
        super("students", "student");
    }

    @Override
    protected Student onProcessSingleEntity(String oneEntityInnerXml) {
        Student SE = new Student();
        SE.setId(parseLong(getWhatIsInside(oneEntityInnerXml, "id")));
        SE.setFirstName(getWhatIsInside(oneEntityInnerXml, "firstName"));
        SE.setLastName(getWhatIsInside(oneEntityInnerXml, "lastName"));
        SE.setAge(parseInt(getWhatIsInside(oneEntityInnerXml, "age")));
        return SE;
    }

    public Students parse(String xml) {
        List<Student> l = super.parseT(xml);
        Students st = new Students();
        st.setStudents(l);
        return st;
    }
}
