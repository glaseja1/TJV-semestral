/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import entities.Teacher;
import entities.Teachers;
import entities.Verb;
import entities.Verbs;
import java.util.List;

/**
 *
 * @author Honza
 */
public class XMLParserVerb extends XMLParser<Verb> {

    public XMLParserVerb() {
        super("verbs", "verb");
    }

    @Override
    protected Verb onProcessSingleEntity(String oneEntityInnerXml) {
        Verb SE = new Verb();
        SE.setId(parseLong(getWhatIsInside(oneEntityInnerXml, "id")));
        SE.setInfinitive(getWhatIsInside(oneEntityInnerXml, "infinitive"));
        return SE;
    }

    public Verbs parse(String xml) {
        List<Verb> l = super.parseT(xml);
        Verbs st = new Verbs();
        st.setVerbs(l);
        return st;
    }
}
