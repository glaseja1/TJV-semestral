/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Honza
 */
@Entity
@Table(name = "verbs")
@XmlRootElement
public class Verb implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    /**
     * The infinitive of the verb
     */
    @Column(name = "INFINITIVE")
    private String infinitive = "none";
        
    /**
     * Students, that know this verb and can conjugate it
     */
    /*@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "student_verb",
        joinColumns = @JoinColumn(name = "verb_id"),
        inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    private List<Student> students;*/
    
    public Verb()
    {
    }
    public Verb(Long ID)
    {
        this.id = ID;
    }
    public Verb(String infinitive)
    {
        this.infinitive = infinitive;
    }
    
    //******************************************************************************************
    
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Verb)) {
            return false;
        }
        Verb other = (Verb) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    
   
    public String getInfinitive() {
        return infinitive;
    }
     /*@XmlTransient
    public List<Student> getStudents() {
        return students;
    }*/

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    /*public void setStudents(List<Student> students) {
        this.students = students;
    }*/
    
    
    public void setId(Long id) {
        this.id = id;
    }
    public void setInfinitive(String s) {
        infinitive = s;
    }

    @Override
    public String toString() {
        return "entities.Verb[ id=" + id + " ]";
    }
}
