/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Honza
 */
@Entity
@Table(name = "teachers")
@XmlRootElement
public class Teacher implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRSTNAMETEACHER")
    String firstName;
    @Column(name = "LASTNAMETEACHER")
    String lastName;
        
    /**
     * Students, that are being teched by this teacher
     */
    //@ManyToMany(mappedBy = "teachers")
//    @JoinTable(name = "student_teacher",
//        joinColumns = @JoinColumn(name = "teacher_id"),
//        inverseJoinColumns = @JoinColumn(name = "student_id")
//    )
    private List<Student> students;
    
    
    public Teacher()
    {
    }
    
    public Teacher(String name, String surname)
    {
        this.firstName = name;
        this.lastName = surname;
    }
    //****************************************************************************************
    public Long getId() {
        return id;
    }
    
    //@Column(name = "FIRSTNAMETEACHER", length = 100)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teacher)) {
            return false;
        }
        Teacher other = (Teacher) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Teacher[ id=" + id + " ]";
    }
    
}
